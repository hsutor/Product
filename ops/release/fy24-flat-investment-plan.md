# FY24 Release flat investment plan

What progress does the Release stage expect to achieve with the current level of investment in FY24?

Note: The following page may contain information related to upcoming products, features and functionality. It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes. Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features or functionality remain at the sole discretion of GitLab Inc. 

## Context

Currently, [the Release team](https://about.gitlab.com/handbook/engineering/development/ops/release/) is fully staffed with 8 engineers (6 BE, 2 FE), TW, EM, PD, PM and UX manager. 

In FY23, our focus was mainly on Environments management, to help customers better understand and manage their deployments to various environments and deployment execution functionality, like deploy approvals. We also focused heavily on reliability, usability, and scalability throughout the year for many of our features. And though we did not achieve maturity upgrades on other categories, we were still able to make minor improvements to Release (the feature) workflows and maintainance and bug fixes for Release Orchestration and Continuous Delivery categories.

## Play for FY24

### Category Focus
- Environment Management
- Release Orchestration
- Supporting Configure Kubernetes Management

### Major Themes
We would 

- launch Environments and K8s Agent integration (cross-stage with Configure)
- reinforce Deployment as a first-class citizen in GitLab
- mature Release Orchestration to Complete
- some dogfooding of features

### Priorities
1. [Iterate on Deploy Approval feature](https://gitlab.com/groups/gitlab-org/-/epics/6832)
1. [Environments integration with K8s Agent](https://gitlab.com/gitlab-org/gitlab/-/issues/352186)
1. [Group Environments](https://gitlab.com/groups/gitlab-org/-/epics/7558)
1. Deployment set up (overlap with configure) - similar to how Harness has a whole UI to configure deployments
1. [Improved rollback experience](https://gitlab.com/gitlab-org/gitlab/-/issues/388298)
1. [Group Releases](https://gitlab.com/groups/gitlab-org/-/epics/3561)
1. [Changelogs/Release notes](https://gitlab.com/groups/gitlab-org/-/epics/2285)
1. [Coordinating Releases/deployments across projects](https://gitlab.com/groups/gitlab-org/-/epics/8483)


### Product metric goals

| Metric | FY24 Start | MoM % growth assumption | Q1 Target | Q2 Target | Q3 Target | Q4 Target / FY24 End |
| ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| Release SMAU | 588k | 3% | 642k | 702k | 767k | 838k |
| Deploy Approval MAU | 1.2k | 10% | 1.6k | 2.1k | 2.8k | 3.8k |
| Number of monthly deployments | 73.7M | 3% | 80M | 88M | 96M | 105M |



## Further considerations

- https://gitlab.com/gitlab-com/Product/-/issues/1634
