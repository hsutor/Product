# FY24 Release +10% investment plan

What impact would a +10% investment in FY24 have on the Release direction? What impact would 1.5 headcount increase have on the Release direction?

Note: The following page may contain information related to upcoming products, features and functionality. It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes. Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features or functionality remain at the sole discretion of GitLab Inc. 

## Context

Currently, [the Release team](https://about.gitlab.com/handbook/engineering/development/ops/release/) is fully staffed with 8 engineers (6 BE, 2 FE), TW, EM, PD, PM and UX manager. An additional 10% investment would result in 9.5 engineers (6.5, 3 FE). I'll assume no team split happens.

In FY23, our focus was mainly on Environments management, to help customers better understand and manage their deployments to various environments and deployment execution functionality, like deploy approvals. We also focused heavily on reliability, usability, and scalability throughout the year for many of our features. And though we did not achieve maturity upgrades on other categories, we were still able to make minor improvements to Release (the feature) workflows and maintainance and bug fixes for Release Orchestration and Continuous Delivery categories.

## Proposal

The additional headcount would be used to accelerate the roadmap and build features for additional categories.

### Planned progress without additional investment

See the [flat investment plan](./fy24-flat-investment-plan.md).

### The effect of additional investment

### Category Focus
- Environment Management
- Release Orchestration
- Supporting Configure Kubernetes Management
- Continuous Delivery (additional category)

### Major Themes

The additional investment would allows us to move more quickly on the roadmap but also fit in additional features in the Continuous Delivery category as well as make more progess on usability. Specific additional features/projects include:

- Making Resource Groups more flexible
- Deploy Freezes
- More progress on usability
- Additional Dogfooding

### Effect on product metrics

| Metric | FY24 Start | MoM % growth assumption (change from flat plan) | Q1 Target | Q2 Target | Q3 Target | Q4 Target / FY24 End |
| ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| Release SMAU | 588k | 3.5% (+.5%) | 652k | 722k | 801k | 888k |
| Deploy Approval MAU | 1.2k | 10% (no change) | 1.6k | 2.1k | 2.8k | 3.8k |
| Number of monthly deployments | 73.7M | 3.5% (+.5%) | 81M | 90M | 100M | 111M |


