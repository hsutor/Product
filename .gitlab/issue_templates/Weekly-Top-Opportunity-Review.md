## :dart: Intent
**To develop a better understanding of Top Opportunities in our Sales Pipeline within the product team.** This issue is a weekly template that Product team members participate in to better understand late-stage (`Commit`) opportunities as well as recent (`Tech Eval`) opportunities. We primarily review them from an angle for what the product team can learn about the types of customers we successfully attract and satisfy and specifics about those individual customer opportunities that helped us get into the late-stage position or pass/fail a tech eval. 

## :rocket: Opportunities to Review (12 Total)
The current pool is (Orit Golowinski, Sarah Waldner, Kevin Chu, Kai Armstrong, Eric Schurter, Torsten Linx, Mark Wood, Hannah Sutor, Brian Rhea, Jocelyn Ellis, Chris Balane and Christina Lohr)

### Commit Opportunities
Current quarter commit (Enterprise and Commercial) opportunities per week. 
- [ ] []() - @
- [ ] []() - @
- [ ] []() - @
- [ ] []() - @
- [ ] []() - @

### Recent Tech Eval Opportunities
- [ ] []() - @
- [ ] []() - @

### Best-Case Opportunities
- [ ] []() - @nagyv-gitlab

## :white_check_mark: Tasks

### :airplane_departure: Quarterly Tasks
Performed the first week of the quarter. 
- [ ] Rename this issue to `Quarterly Summary - Top Opportunity Review - FYXXQX` - @nagyv-gitlab
- [ ] Don't investigate new opportunities, start a thread asking participating team members to look back across previous reviews for themes - @nagyv-gitlab
- [ ] Solicit new PMs to participate - @nagyv-gitlab
- [ ] Consider other opportunity types for investigation (losses) - @nagyv-gitlab
- [ ] Propose a summary and ping `@gl-product-leadership` for additional input - @nagyv-gitlab
- [ ] After review update the description of the issue with a `Quarterly Highlights` summary of themes from the quarter - @nagyv-gitlab
- [ ] Share the `Quarterly Highlights` with the company, specifically via [a Field Announcement](https://about.gitlab.com/handbook/sales/field-communications/#requesting-field-announcements) and possibly including in the Earnings Call - @nagyv-gitlab

<details><summary>Quarterly Templates</summary>

#### Request for summaries
```
@PARTICIPANTS You all participated at various points through the last quarter. I'd like to invite you to summarize any trends you noticed during your reviews OR that you do notice from a review of previous issues from this quarter (Week X-Y). Please add your summary of trends to in the thread below. 🙇
```

#### Summary
```
TBD
```

#### Solicit new participants
```
👋 Hi! Every week a group of PMs each review a single Sales Opportunity to get some personal exposure to why customers are choosing GitLab broadly, and to provide insight to the broader Product org. The lift is pretty lightweight, with one async process each week that takes about 15 minutes of time. You can opt-out at any time (or pause if you have urgent priorities). If you are interested in participating this quarter please ping me in this thread. 🙇
```

</details>

### :o: Opening Tasks
- [ ] Create retrospective thread - @nagyv-gitlab
- [ ] Determine un-investigated opportunities (using the [Opportunity Tracker](https://docs.google.com/spreadsheets/d/1ZLDVAYnZXyt22zGVyvd26i0ULLMG0hHFNHg-xKSYP9g/edit#gid=0)) from [Enterprise Commit](https://gitlab.my.salesforce.com/00O4M000004aZ98), [Commercial Commit](https://gitlab.my.salesforce.com/00O8X000008QktT)(note some team members won't have access), [Tech Eval](https://gitlab.my.salesforce.com/00O4M000004acjz) and [best-case](https://gitlab.my.salesforce.com/00O4M000004aj4Q) opportunities and assign based on counts in the issue template - @nagyv-gitlab
- [ ] Quickly Check to ensure Commit opportunities have a command plan (dollar amount might impact this) - @nagyv-gitlab
- [ ] Assign to team members for individual updates
- [ ] Mark Assigned Opportunities in the [Opportunity Tracker](https://docs.google.com/spreadsheets/d/1ZLDVAYnZXyt22zGVyvd26i0ULLMG0hHFNHg-xKSYP9g/edit#gid=0)

### :crown: Assigned Opportunity Tasks
- Understand by checking the SFDC opportunity (specifically the [command plan](https://about.gitlab.com/handbook/sales/command-of-the-message/command-plan/)), [GitLab Account Projects](https://gitlab.com/gitlab-com/account-management/), [account GDrive folder](https://drive.google.com/drive/u/0/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U), slack channel and meeting notes
- Add a comment with the following format:

#### For Commit Opportunities
```
## Commit Opportunity - ACCOUNT_NAME

[Account_Name](Opportunity_Link) - REPLACE ME - brief description of the business (what do they do, how are they organized, what will they use GitLab for, website or wikipedia link) and opportunity (new business, upgrade, expansion, tech eval result)

Highlights from [Command Plan](CP_Link) (PM's mentioned as FYI):

1. Highlight - PM

Insights for the team. If warranted tasks for additional updates in slack or handbook updates.

:wave: Hi [ACCOUNT TEAM] - you can check out the [intent](#dart-intent) of this issue, here are some questions I have about this opportunity:

1. Question
```

#### For Tech Eval Opportunities
```
## Tech Eval Opportunity - ACCOUNT_NAME

[Account_Name](Opportunity_Link) - REPLACE ME - brief description of the business (what do they do, how are they organized, what will they use GitLab for, website or wikipedia link) and opportunity (new business, upgrade, expansion, tech eval result)

Highlights from [Command Plan](CP_Link) (PM's mentioned as FYI):

1. Highlight - PM

:wave: Hi [ACCOUNT TEAM] - you can check out the [intent](#dart-intent) of this issue, here are some questions I have about this opportunity:

- Why we won/lost?
- Any hurdles that did or did not overcome in the eval?
- Any required capabilities that were part of the eval that we didn't match?
- Any capabilities that we show-cased that really won over the customer?
```

- Post insights in appropriate channels

#### :up: Individual Updates
Unassign yourself when your above tasks are complete.

### :x: Closing Tasks
* [ ] Remove any uninvestigated opportunities from the [Opportunity Tracker](https://docs.google.com/spreadsheets/d/1ZLDVAYnZXyt22zGVyvd26i0ULLMG0hHFNHg-xKSYP9g/edit#gid=0) - @nagyv-gitlab
* [ ] Add a summary/highlights comment (Ping `gl-product-leadership`, David Sakomoto and Jonathan Fullham) and share it in Slack on #product channel (ping `@cs_leaders`) - @nagyv-gitlab
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Top-Opportunity-Review.md) based on the retrospective thread - @nagyv-gitlab
* [ ] #thank all GTM team members who participated - @nagyv-gitlab

## :book: References
- [SalesForce Current Quarter Opportunity Dashboard](https://gitlab.my.salesforce.com/01Z4M0000007H7W)
- [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=opportunity+review&scope=all&sort=created_date&state=closed&utf8=%E2%9C%93)

### :1234: Opportunity Types/Lists
  - [Commercial First Order Commit Opportunities](https://gitlab.my.salesforce.com/00O8X000008QktT) - Late stage commercial opportunities that have command plan filled out and are first order.
  - [Enterprise Commit Opps](https://gitlab.my.salesforce.com/00O4M000004aZ98) - These are late-stage opportunities that are past the point of technical win and likely to close. The "commit" indicate that the account teams have a high degree of confidence in close. We review these to understand why we are likely to win.
  - [Tech Eval Opps](https://gitlab.my.salesforce.com/00O4M000004acjz) - These are mid-stage opportunities that have recently completed Tech Eval. Sometimes this is successful and sometime it is not. We review these to understand why we technically win or lose.
  - [Best-case Opps](https://gitlab.my.salesforce.com/00O4M000004aj4Q) - These are early-stage opportunities and we target particularly large ones. We offer to help frame and technically win these critical deals that aren't likely to close in current quarter.

/confidential

/label ~"opportunity review" 

/assign @nagyv-gitlab

/due Thursday
