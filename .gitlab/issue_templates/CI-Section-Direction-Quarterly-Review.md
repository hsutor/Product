## Intent
Quarterly [CI Section Direction](https://about.gitlab.com/direction/ci/) Review and AMA

## Everyone Can Contribute
- Please see the [GoogleDoc for contribution instructions](https://docs.google.com/document/d/1Brh_clrV9UbMBYznQic2ZjRGu5NcpxexDvfGR374l_4/edit?usp=sharing), you can contribute synchronously or synchronously.

## Tasks
- [ ] Update contribution method in doc
- [ ] Schedule AMA/Review
- [ ] Communicate to team
- [ ] Followup tasks
- [ ] Followup on retro thread