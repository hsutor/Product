**Data collection period**: **month/day - month/day** – **month/day - month/day**

[Slides]() `@brhea`

[Raw analysis spreadsheet]() `@brhea`

## Opening tasks

- [ ] Update collection period dates above `@fseifoddini`
- [ ] Update due dates of the task list below `@fseifoddini`
- [ ] Assign a due date to this issue `@fseifoddini`
- [ ] Assign a milestone to this issue `@fseifoddini`
- [ ] Ping to `@brhea `and `@dpeterson1` to check dates and kickoff `@fseifoddini`

## Survey To Dos

- [ ] By **month/day - month/day**: Create analysis slides template, update slides owned by ProdOps, and paste link above `@brhea`
  - Save this to [PPS survey folder](https://drive.google.com/drive/folders/1LPa_4qaSW8NuKA90yIapeD2ztNyM0cNH) on gdrive
  - Give access to the appropriate collaborators
- [ ] By **month/day - month/day**: Export and link raw data sheet above `@brhea`
  - Login to Qualtrics and choose the Post-Purchase Survey project
  - Go to "Data & Analysis"
  - Add a filter for "Survey Metadata > Start Date > Between" and enter the first and last date of the data collection period
  - Apply the filter
  - Choose "Export Data" from the "Export & Import" dropdown menu
  - Download all fields in CSV format and select the "Use choice text" option
  - [ ] Save the exported file to [PPS survey folder](https://drive.google.com/drive/folders/1LPa_4qaSW8NuKA90yIapeD2ztNyM0cNH) on gdrive
  - [ ] Give access to the appropriate collaborators
- [ ] By **month/day - month/day**: Complete feature count slides: `@brhea`
- [ ] By **month/day - month/day**: Complete ARR slides `@dpeterson1`
- [ ] By **month/day - month/day**: Complete ARR slides `@dpeterson1`
- [ ] **month/day - month/day** - **month/day - month/day**:  Final review of all slides `@brhea` and ping `@fseifoddini` in slides for final review
- [ ] By **month/day - month/day**: Final review `@fseifoddini` and ping VP Product in slides for final review
- [ ] By **month/day - month/day**: Share out with Product, UX, Dev, Sales, CS, Marketing, Product Marketing, Quality Assurance by posting in Slack `@fseifoddini`
     - CC  `@brhea and @dpeterson1` in the Slack post
- [ ] After x-posting above has happened, share out in  Slack CEO channel `@fseifoddini `

**Tasks below should be completed within two weeks of sharing out the analysis across teams:** 

- [ ] Update `features.yml` / [feature by theme page](https://about.gitlab.com/features/by-theme/) with the latest feature counts, following [these instructions](https://about.gitlab.com/handbook/product/product-operations/surveys/workflows/#how-to-update)  `@brhea`
- [ ] Random drawing: `@brhea`
- [ ] Send prize to winner: `@brhea`
- [ ] Add link to analysis slides to  [prod ops survey page](https://about.gitlab.com/handbook/product/product-operations/surveys/) `@brhea`
- [ ] Add link to analysis slides to read-only section of PM biweekly meeting `@fseifoddini`

/label ~"workflow::In dev" ~"Product Operations" ~"prodops:direction" ~"prodops:feedbackloops"
/confidential
/assign @brhea @fseifoddini @dpeterson1
