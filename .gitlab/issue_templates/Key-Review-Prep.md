**Next Product Key Review meeting date:**

**Slide due date:**

**Meeting Agenda:**
https://docs.google.com/document/d/1Hmg2r-VGYUtYqlHDoPCzeJPI350Y58JSGVHy-P33UBc/edit

**Link to Google slides for this month's Key Revew - Product:**

**Link to Google Folder with all Decks:**
https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg

### Opening Tasks: 

* [ ] `@justinfarris` - Add meeting date and slides due date above
* [ ] `@justinfarris` - Update issue title to reference date YYYY/MM/DD of upcoming key review
* [ ] `@justinfarris` - Assign a due date to this issue 
* [ ] `@justinfarris` - Add the previous month prep issue to this issue via `Linked items`
* [ ] `@justinfarris` - Add working draft of slides to issue description 
* [ ] `@justinfarris` - Add meeting date to first page of Key Review slides

### Slide Update Tasks

#### Review and update the Business Driver slides
  * [ ] `@justinfarris` - make sure projects and owners in slides are update to date; update "up to date" comment on intro slide; tag in the appropriate team members into the slides for updates as necessary

#### Review and update Cross-Functional Project Update slides
  * [ ] `@justinfarris` - make sure projects and owners in slides are up to date; tag in the appropriate team members below for updates as necessary
the slides

#### Review and update Dedicated Project Update slides
  * [ ] `@justinfarris` - make sure projects and owners in slides are up to date; tag in the appropriate team members below for updates as necessary
the slides

#### Review and update OKR Updates slides
  * [ ] `@justinfarris` - make sure OKRs and owners in slides are in sync with GitLab SSOT and tag in owners of  "behind" or "at-risk" KRs into the appropriate slides to add updates to the slides

#### Review and update Appendix Metrics
  * [ ] `@justinfarris` - - make sure projects and owners in slides are update to date; update "up to date" comment on intro slide; tag in team members below for updates as necessary
  * [ ] `@justinfarris` - check/update your metrics if needed
  * [ ] `@stkerr` - check/update your metrics if needed

#### Review full deck before Key Meeting

 * [ ] Run `/assign  @gschwam @jennifergarcia20 @justinfarris @david @joshlambert @hbenson @fzimmer @jreporter @ofernandez2 @ogolowinski @stkerr @clenneville @susantacker @vkarnes @asmolinski2 @mflouton` to add everyone to issue to review 
    * [ ] `@justinfarris`
    * [ ] `@david`
    * [ ] `@hbenson`
    * [ ] `@joshlambert`
    * [ ] `@jreporter`
    * [ ] `@fzimmer` 
    * [ ] `@ofernandez2`
    * [ ] `@asmolinski2`
    * [ ] `@vkarnes `
    * [ ] `@susantacker`
    * [ ] `@clenneville`
    * [ ] `@mflouton`

#### Closing Tasks
* [ ] All - After each Key Meeting [update this Key Review prep issue template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Key-Review-Prep.md) with any added items
* [ ] Anyone - Add a "Retrospective Thread" comment to this issue
* [ ] `@justinfarris` - For next month, make a copy of this month's Key Review Slides and save them in the [Key Review - Product Folder](https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg) 
* [ ] `@justinfarris` - resolve all the retro feedback as needed and close this issue

/confidential
/label ~"Product Operations"
/label ~"workflow::In dev"

