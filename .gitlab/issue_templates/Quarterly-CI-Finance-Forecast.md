## :dart: Intent

Ensure our upcoming quarterly financial forcast for Runner SaaS will be as accurate as possible.

## Tasks

### :wave: Open
* [ ] Create Retrospective Thread - @gabrielengel_gl
* [ ] Give overview of any upcoming changes to Runner SaaS - @gabrielengel_gl
* [ ] Include SRE team to forcast any changes from reliablity with cost implications - @gabrielengel_gl
* [ ] Review of expected usage for upcoming quarter - @gabrielengel_gl
* [ ] Request review from `@mflouton` and `@jreporter` - @gabrielengel_gl
* [ ] Request financial forcast review from `@sshackelford`, `@rtaira`, and `@clem.lr` - @gabrielengel_gl
* [ ] Financial forecast based on respected usage for upcoming quarter - `@sshackelford`, `@rtaira`
* [ ] Financial review of expected usage for upcoming quarter - `@clem.lr`

## Usage Forecast

- [Usage forecast spreadsheet](https://docs.google.com/spreadsheets/d/1hROX1hzJjt2l757JtKagrgizBDcTEzWzq1IGgCrHnCo)

## Upcoming changes
<!--Any changes such as features, bugfixes or maintenance that are predicted to be implemented within the next quarter should be put here.-->

### Cost Increase

### Cost Optimization


cc @jreporter

See the [issue template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Quarterly-CI-Finance-Forecast.md) for any changes.

/assign @gabrielengel_gl
/due in 5 days
/confidential
/label ~"group::runner saas" ~"Category:Runner SaaS" ~"devops::verify" ~"section::ci" ~"keep confidential" 