## Introduction 

Two sub-values at GitLab are [sharing](https://about.gitlab.com/handbook/values/#share) and [findability](https://about.gitlab.com/handbook/values/#findability). In the Product Division (Product Management, UX Research, Product Design, and Tech Writing), our Product Monthly meeting has 60 minutes of show & tell across the teams. We will use this issue to plan those slots.

This month each presenting group has 10 minutes per function. There will be a block of 15 minutes for Q&A at the end of all the sessions. 

## Opening Issue Tasks 

- [ ] Update title with month - @jreporter 
- [ ] Add retrospective thread - @jreporter 
- [ ] Confirm assignees are stage leaders - @jreporter
- [ ] Set due date for 1 week before Product Direction Showcase - @jreporter 
- [ ] Confirm meeting is scheduled with Zoom  - @jreporter 
- [ ] Update [Agenda](https://docs.google.com/document/d/13BuRLiDJpNNhGmO1dOPqMQR0yaEDZY4CFGFR49tZMYo/edit?usp=sharing)  - @jreporter 

## Participant/Speaker Actions to take 

- [ ] Add Name and department to the table 
- [ ] Add topic and links to the table 
- [ ] Request async review of materials 24 hours before meeting - @jreporter 

### Stage order table 

| Order | Presenter Name  | Department  | Topic & Links  |
| --- | --- | --- | --- |
| 1 |  |   |  | 
| 2 |  |  |  |  
| 3 |  |  |  |  
| 4 |  |  |  |  

## Closing Issue Tasks 

- [ ] Resolve any retrospective items by updating [issue template ](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/product_division_showcase.md) - @jreporter 
- [ ] Identify next month's presenters in a comment thread "Next Month Presenter Thread" - @jreporter 
- [ ] Share recording in #product, #whats-happening-at-gitlab  - @jreporter 
- [ ] Add Livestream to [Product Team YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko7HO427nXkoz9kovi_2dBi) - @jennifergarcia20 
- [ ] Close Issue  - @jreporter 

/assign @jreporter @susantacker @vkarnes @asmolinski2 @jennifergarcia20 
/due in 14 days 
