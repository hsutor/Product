## :dart: Intent

Ensure our product leaders develop a deeper undestanding of performance indicators across sections. This issue is used to remind us to [update the handbook with the current quarterly assignment of section](https://about.gitlab.com/handbook/product/product-leadership/#product-section-leaders-performance-indicator-rotation) that will include each shadowing product leader attending Monthly PI reviews for that section.

## Tasks

### :wave: Open
* [ ] Create Retrospective Thread - @jennifergarcia20
* [ ] Propose Quarterly Assignment between section leaders - @jennifergarcia20
* [ ] Request review of proposal from Section Leaders - @jennifergarcia20

### Quarterly Section Assignments
Available assignments (Orit, Hillary, Kenny, Josh, Omar, SamA)
- Dev Section - Shadow - TBD
- Sec & ModelOps Sections - Shadow - TBD
- Ops Section - Shadow - TBD
- Core Platform Section - Shadow - TBD
- Fulfillment Section - Shadow - TBD
- Growth Section - Shadow - TBD

## Closing
* [ ] [Update the handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/sites/handbook/source/handbook/product/product-leadership/index.html.md.erb) with the current rotation for the quarter
* [ ] Ping Jen Garcia to make appropriate invite adjustments for Shadows and removing non-shadows.
* [ ] Make [updates to this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Quarterly-Product-Leadership-PI-Pairing.md) based on retrospective thread - @jennifergarcia20

FYI @david @justinfarris

## :book: References
- [Handbook Content](https://about.gitlab.com/handbook/product/product-leadership/#product-section-leaders-performance-indicator-rotation)

/assign @jennifergarcia20

/due in 5 days
