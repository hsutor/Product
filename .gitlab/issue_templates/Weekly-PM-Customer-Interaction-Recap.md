## :dart: Intent
**To remind yourself to properly reflect and record findings from the week. It also serves as a way to share details in weekly team meeting, #pm-standup or other places that ask about weekly customer interactions.**

## :book: Customer information gathered this week
<!--- 
What direct, or indirect, interactions or information did you gather about customers this week?
Link to the detailed resources as appropriate. Examples - google doc agenda, dovetail, chorus, user-interviews project issue https://gitlab.com/gitlab-com/user-interviews/-/issues 
--->

* 
* 

## :white_check_mark: Tasks

- [ ] Notate findings in applicable backlog items
- [ ] Create backlog items as applicable

### Trends
<!---  Review this as part of the larger pattern, is any new trend emerging you should take note to watch? --->

/confidential

/assign @nicoleschwartz
