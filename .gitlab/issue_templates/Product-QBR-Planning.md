## :dart: Intent
Every quarter, the Sales team will ping various teams in a [field issue](gitlab-com/sales-team/field-operations/enablement#1578) to kickoff [QBR](https://about.gitlab.com/handbook/sales/qbrs/) sessions/planning. 

This issue will be used to to coordinate attendance, document and disseminate key insights from Product Leaders in QBRs happening in FYXX-QX. The tasks below will be driven by EBA to VPP `@jennifergarcia20` (or Product Operations `@fsefoddini` if EBA unavailable)

## Tasks

Before the sessions: `@jennifergarcia20`

- [ ] Once you're tagged in the current quarter field issue (such as [field issue](gitlab-com/sales-team/field-operations/enablement#1578)), add it as a Reference Link below as well as a `Linked item` to this issue
- [ ] Find the schedule in the field issue, add it as a reference Link below  
- [ ] Update the title of this issue to include the dates of the QBR eg. _Product QBR FY23-Q3 - July 27- Aug. 15_
- [ ] Using Product [QBR Common Notes Template](https://docs.google.com/document/d/1PLOQkft8vriOhQ7TAPsUv5X5lLpV8Mye96Cl7NIQRF4/edit) in the [GitLab Gallery ](https://docs.google.com/document/u/0/?ftv=1&tgif=d) create a notes doc and add session titles ordered by Date and Time  `@jennifergarcia20`
     - Previous [example for reference](https://docs.google.com/document/d/1OjGLdn5by3__n7i4ysc25Mml2pitNH30AE97Y4fAaNA/edit#)  
- [ ] Ping `@gl-product-leadership` for signup and attendance to ensure coverage on critical QBRs. Ask Product Leaders to tag in their PMs as needed. 
- [ ] Send reminder to Product Leadership 7 days prior to QBRs and ask for volunteers for the "After the sessions:" tasks below

During the sessions:

- [ ] Add highlights and takeaways to a [common notes doc](https://docs.google.com/document/d/10l1uR4w1qKT7z4Wsrlec5875gYdfuBybArgRlb_-irE/edit#) - All attendees

After the sessions:

- [ ] Summarize key takeaways from the commons notes doc and document in [the internal handbook](https://internal-handbook.gitlab.io/handbook/product/investment/field-feedback/) - `@mention`
- [ ] Add to PM meeting agenda and present - `@mention`
- [ ] Add/resolve retro (:thumbsup: :thumbsdown: :bulb:) items to this issue as individual comments - ALL

Reference notes and related issues:

- `[FYXX QX field issue](ADD issue link)`
- `[QBR schedule](ADD issue link)`

/label ~"workflow::In dev" ~"Product Operations"
/assign @fseifoddini @jennifergarcia20
