# frozen_string_literal: true

require 'date'
require 'time'
require 'slack-ruby-client'
require 'yaml'
require 'optparse'
require 'erb'
require_relative 'milestones'

class Slackbot
  include Milestones

  def initialize
    configure_slack_api
    @client = Slack::Web::Client.new
    @today = Date.today
    @messages = YAML.load_file('config/slackbot_messages.yml')
    @options = {}
    parse_options
    set_messages_to_send
  end

  def parse_options
    OptionParser.new do |opts|
      opts.banner = "Usage: ./prod_ops_slackbot.rb [--dry-run]"

      opts.on("-d", "--dry-run", "Dry Run") do |opt|
        @options[:dry_run] = opt
      end
    end.parse!
  end

  def self.start
    new.send_messages
  end

  def generate_messages(filepath)
    blocks_file = "templates/slack_blocks/#{filepath}"
    if File.extname(blocks_file) == ".erb"
      year = @today.year
      month = @today.strftime('%m') # NOTE(csouthard): we need a zero padded month in the url
      milestone_date = Date.new(year, month.to_i, 22)
      milestone = html_sanitize_milestone(date_to_milestone(milestone_date.strftime("%B %d, %Y").to_s))
      template = ERB.new(File.read(blocks_file))
      message_block = YAML.load(template.result(binding))
    else
      message_block = YAML.load_file(blocks_file)
    end
    message_block
  end

  def send_messages
    @messages_to_send.each do |message|
      message["channel"].map! { |channel| @options[:dry_run] ? "prodops-slackbot-test" : channel }

      message["channel"].each do |channel|
        @client.chat_postMessage(channel: channel, blocks: generate_messages(message['blocks_file']))
        notify_prodops_of_sent_message(message, channel)
      end
    end
  end

  private

  def configure_slack_api
    Slack.configure do |config|
      config.token = ENV['PRODOPS_SLACKBOT_API_PRIVATE_TOKEN']
    end
  end

  def set_messages_to_send
    @messages_to_send = @options[:dry_run] ? @messages : @messages.select { |message| schedule_match?(message) }
  end

  def schedule_match?(message)
    case message.dig("schedule", "type")
    when "weekly"
      message.dig("schedule", "day_of_week") == @today.strftime("%A") && (message.dig("schedule", "utc_hour_of_day") || 0) == Time.now.utc.hour
    when "monthly"
      message.dig("schedule", "day_of_month") == @today.day && (message.dig("schedule", "utc_hour_of_day") || 0) == Time.now.utc.hour
    when "annual"
      message.dig("schedule", "month") == @today.strftime("%B") && message.dig("schedule", "day_of_month") == @today.day && (message.dig("schedule", "utc_hour_of_day") || 0) == Time.now.utc.hour
    end
  end

  def notify_prodops_of_sent_message(message, channel)
    @client.chat_postMessage(channel: 'prodops-slackbot-test', text: ":gitlab-bot: *Sent message:* #{message['name']} to ##{channel}")
  end
end
